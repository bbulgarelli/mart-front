import { useEffect, useState } from 'react';
import { ActivityIndicator,  Image,  ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Carrossel from './components/Carrocel';
import Header from './components/Header';
import OpcoesHeader from './components/OpcoesHeader';
import productCardModal from './components/ProductCardModal';

const leftArrow = require('./assets/icons/arrow-left-solid.png')

export default function App() {
  const [data, setData] = useState(null)
  const [isLoading, setLoading] = useState(true)

  const [isLoadingSearch, setLoadingSearch] = useState(true)
  const [isSearching, setSearching] = useState(false)
  const [searchData, setSearchData] = useState(null)


  const getProduct = async () => {
    let res = await fetch('http://localhost:8000/categoria/464300220006')
                .then(response => response.json())
                .catch(err => {return null})
    if(res === null)
      return
    for(let i=0; i<res.length; i++){
      res[i].id = i
    }
    setData(res)
    setLoading(false)
  }

  const searchProduct = async (produto) => {
    setLoadingSearch(true)
    setSearching(true)
    let res = await fetch(`http://localhost:8000/busca/${produto}`)
                .then(response => response.json())
                .catch(err => {return null})
    if(res === null)
      return
    for(let i=0; i<res.length; i++){
      res[i].id = i
    }
    setSearchData(res)
    setLoadingSearch(false)
  }

  useEffect(() => {
    getProduct()
  }, [])

  return (
    <>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.returnArrowContainer}>
          { isSearching &&
          <TouchableOpacity onPress={() => {setSearching(false)}}>
            <Image source={leftArrow} style={styles.returnArrow}></Image>
          </TouchableOpacity>
          }
        </View>
        <Header onSubmit={searchProduct}></Header>
        {!isSearching && 
        <>
          <OpcoesHeader></OpcoesHeader>
          <View style={styles.maisVendidos}>
            <Carrossel data={data} productCard={productCardModal} title={"Especialmente, pra vc ;)"}></Carrossel>
            {isLoading &&
                  <ActivityIndicator size="large" color="#ff0000"></ActivityIndicator>
            }
          </View>
        </>
        }
        {isSearching &&
        <>
          <Carrossel data={searchData} productCard={productCardModal} title={"Produtos encontrados"}></Carrossel>
          {isLoadingSearch &&
            <ActivityIndicator size="large" color="#ff0000"></ActivityIndicator>
          }
        </> 
        }
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingBottom: 30
  },
  maisVendidos: {
    marginTop: 10,
    backgroundColor: 'yellow',
    paddingTop: 10,
    height: '100%'
  },
  returnArrowContainer: {
    backgroundColor: 'red',
    width: '100%',
    padding: 5
  },
  returnArrow: {
    width: 20,
    height: 20,
    resizeMode: 'contain'
  }
});
