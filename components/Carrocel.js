import { FlatList, StyleSheet, Text, View } from 'react-native';

export default function Carrossel(props) {
  return (

    <View style={styles.comprouComprou}>
        <View style={styles.comprouComprouTitle}>
            <Text style={styles.comprouComprouText}>{props.title}</Text>
        </View>
        <View style={styles.carrossel}>
            <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={props.data}
                renderItem={({item}) => {return(<props.productCard name={item.nome} price={item.preco} pits={item.pits}></props.productCard>)}}
                keyExtractor={item => item.id}
            />
        </View>
    </View>
  );
}

const styles = StyleSheet.create({

    comprouComprou: {
        width: '100%',
    },
    comprouComprouTitle: {
        height: 35,
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'red',
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    comprouComprouText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold'
    },
    carrossel: {
        margin: 10
    }
});
