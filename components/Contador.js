import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export default function Contador() {
    const [counter, setCounter] = useState(0)


    return (
        <View style={styles.container}>
            <View style={styles.counterOptions}>
                <TouchableOpacity onPress={() => setCounter(counter+1)}>
                    <Text style={styles.text}>+1</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setCounter(counter+2)}>
                    <Text style={styles.text}>+2</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setCounter(counter+3)}>
                    <Text style={styles.text}>+3</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setCounter(counter+10)}>
                    <Text style={styles.text}>+10</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.counter}>
                <TouchableOpacity onPress={() => {
                    if(counter > 0)
                        setCounter(counter-1)
                }}>
                    <Text style={styles.text}>-</Text>
                </TouchableOpacity>
                <View>
                    <Text style={styles.text}>{counter}</Text>
                </View>
                <TouchableOpacity onPress={() => setCounter(counter+1)}>
                    <Text style={styles.text}>+</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center',
        marginBottom: 20
    },
    counterOptions: {
        flexDirection: 'row',
        padding: 10,
        paddingHorizontal: 40,
        borderRadius: 15,
        backgroundColor: 'red',
        width: '80%',
        justifyContent: 'space-between'
    },
    text: {
        color: 'white',
        fontSize: 20
    },
    counter: {
        marginTop: 15,
        flexDirection: 'row',
        padding: 10,
        paddingHorizontal: 20,
        borderRadius: 15,
        backgroundColor: 'red',
        width: '30%',
        justifyContent: 'space-between'
    }
});