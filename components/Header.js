import React from 'react';
import { StyleSheet, Image, View, TextInput, Text } from 'react-native';


const pinLocation = require('../assets/icons/pin-location.png');
const circle = require('../assets/icons/circle.png');
const extend = require('../assets/icons/extend-arrow.png');

export default function Header(props) {
    const [text, onChangeText] = React.useState(null)

    return (
      <>
        <View style={styles.container}>
            <TextInput
            placeholder='Buscar'
            style={styles.input}
            onChangeText={onChangeText}
            value={text}
            onSubmitEditing={() => props.onSubmit(text)}
            />
            <View style={styles.location}>
                <Image source={pinLocation} style={styles.pinLocation} />
                <View style={{flexDirection: 'row'}}>
                  <Image source={circle} style={styles.circleLocation} />
                  <Text style={styles.textLocation}>Rua Marquês de São Vicente, Gávea</Text>
                </View>
                <Image source={extend} style={styles.extendLocation} />
            </View>
        </View>
        <View style={styles.linha}></View>
      </>
    );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red',
    width: '100%',
    paddingHorizontal: 30,
    paddingVertical: 10
  },
  input:{
    borderRadius: 10,
    padding: 3,
    paddingLeft: 15,
    backgroundColor: 'white'
  },
  location:{
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: 10
  },
  pinLocation:{
    width: 22,
    height: 30,
    resizeMode: 'stretch'
  },
  circleLocation:{
    width: 10,
    height: 10,
    resizeMode: 'stretch',
    marginLeft: 15,
    marginTop: 10
  },
  textLocation:{
    color: 'white',
    marginLeft: 8,
    marginTop: 7,
    fontSize: 13,
    height: 20
  }, 
  extendLocation:{
    width: 20,
    height: 10,
    resizeMode: 'stretch',
    marginLeft: 20,
    marginTop: 10
  },
  linha:{
    height: 5,
    width: '100%',
    backgroundColor: '#780800'
  }
});