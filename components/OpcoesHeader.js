import React from 'react';
import { StyleSheet, Image, View, Text } from 'react-native';

const iconeCheckList = require('../assets/icons/icone-checklist-colorido.png')
const cupomAme = require('../assets/image/cardpromoame.png')

export default function OpcoesHeader() {

    return (
        <>
        <View style={styles.container}>
            <View style={styles.optionBox}>
                <Text style={styles.text}>Sua lista</Text>
                <Image source={iconeCheckList} style={styles.checkList}></Image>
            </View>
            <View style={styles.optionBox}>
                <Text style={styles.text}>Sua cesta</Text>
            </View>
        </View>
        <View style={styles.containerCupom}>
            <Image source={cupomAme} style={styles.cupomAme}></Image>
        </View>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 50,
        paddingVertical: 10,
        backgroundColor: 'red'
    },
    optionBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#c71616',
        paddingHorizontal: 20,
        borderRadius: 15
    },
    text: {
        color: 'white',
        fontSize: 17,
        marginRight: 15
    },
    checkList: {
        width: 30,
        height: 40
    },
    containerCupom: {
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    cupomAme: {
        width: '100%',
        height: 200,
        resizeMode: 'contain',
        borderRadius: 15,
        overflow: 'hidden'
    }
});