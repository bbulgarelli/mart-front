import { StyleSheet, Text, View, Image } from 'react-native';

const PlaceholderImage = require('../assets/icon.png');

export default function ProductCard(props) {
  return (
    <View style={styles.container}>
        <View style={styles.header}>
            <Text style={styles.price}>R$ {props.price} cada</Text>
        </View>
        <View  style={styles.imageWrap}>
            <Image source={PlaceholderImage} style={styles.image} />
        </View>
        <View style={styles.footer}>
            <Text style={styles.title}>{props.name}</Text>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f0f0f0',
    flexDirection: 'column',
    width: 140,
    padding: 5,
    margin: 5,
    height: 230
  },
  header:{
    margin: 3,
    flex: 1,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  price:{
    color: 'red',
    backgroundColor: 'yellow',
    padding: 3
  },
  image:{
    width: 120,
    height: 120,
    resizeMode: 'contain'
  },
  footer:{
    backgroundColor: '#ff0000',
    padding: 3,
    height: 60
  },
  title:{
    color: 'white'
  },
  imageWrap:{
    height: 130,
  },
  plus:{
    height: '100%',
    paddingBottom: 5
  },
  plusFont:{
    textAlign: 'center',
    fontSize: 30,
    color: 'red'
  }
});