import { useState } from 'react';
import { TouchableOpacity, Modal } from 'react-native';
import ProductCard from './ProductCard';
import ProductPage from './ProductPage';

const PlaceholderImage = require('../assets/image/kitkat.png');

export default function productCardModal(props) {
  const [isVisible, setIsVisible] = useState(false)
  const [isLoading, setLoading] = useState(true)
  const [qcc, setQcc] = useState(null)
  const [qvc, setQvc] = useState(null)

  const hideModal = () => {
    setIsVisible(false)
  }

  const fetchData = async () => {
    let resQcc = await fetch(`http://localhost:8000/qcc/${props.pits}`)
                .then(response => response.json())
                .catch(err => {return null})
    let resQvc = await fetch(`http://localhost:8000/qvc/${props.pits}`)
                .then(response => response.json())
                .catch(err => {return null})

    if(resQcc !== null){
      for(let i=0; i<resQcc.length; i++){
        resQcc[i].id = i
      }
    }
    
    if(resQvc !== null){
      for(let i=0; i<resQvc.length; i++){
        resQvc[i].id = i
      }
    }


    setQcc(resQcc)
    setQvc(resQvc)
    setLoading(false)
  }

  return (
    <>
    <TouchableOpacity onPress={()=>{
      setIsVisible(true)
      fetchData()
    }}>
      <ProductCard name={props.name} price={props.price}></ProductCard>
    </TouchableOpacity>

      <Modal 
        animationType='slide' 
        visible={isVisible} 
        onRequestClose={() => setIsVisible(false)}>
        <ProductPage onClose={hideModal} name={props.name} price={props.price} qcc={qcc} qvc={qvc} isLoading={isLoading}></ProductPage>
      </Modal>
    </>
  );
}
