import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';
import Carrossel from './Carrocel';
import Contador from './Contador';
import ProductCard from './ProductCard';

const returnArrow = require('../assets/icons/extend-arrow-red.png')
const iconeCheckList = require('../assets/icons/icone-checklist-colorido.png')
const product = require('../assets/icon.png')

export default function ProductPage(props) {

  return (
    <ScrollView contentContainerStyle={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.returnArrowView} onPress={() => props.onClose()}>
                    <Image source={returnArrow} style={{width: 30, height: 30, marginLeft: 20}}></Image>
                </TouchableOpacity>
                <View style={styles.productImageView}>
                    <Image source={product} style={{width: 200, height: 200}}></Image>
                </View>
                <View style={styles.checkListView}>
                    <View style={styles.checkListContainer}>
                        <Image source={iconeCheckList} style={styles.checkList}></Image>
                    </View>
                    <View style={styles.price}>
                        <Text style={styles.priceText}>R$ {props.price}</Text>
                    </View>
                </View>
            </View>
            <View style={styles.title}>
                <Text style={styles.titleText}>{props.name}</Text>
            </View>
            <View style={styles.disclaimerView}>
                <Text style={styles.disclaimer}>imagem meramente ilustrativa</Text>
            </View>
            <View style={styles.buyButton}>
                <Text style={styles.buyButtonText}>Comprar</Text>
            </View>
            <Contador></Contador>
            <Carrossel productCard={ProductCard} title={"Quem comprou, tbm comprou ;)"} data={props.qcc}></Carrossel>
            {   props.isLoading && 
                <ActivityIndicator size="large" color="#ff0000"></ActivityIndicator>
            }
            <Carrossel productCard={ProductCard} title={"Quem viu, tbm comprou ;)"} data={props.qvc}></Carrossel>
            {   props.isLoading && 
                <ActivityIndicator size="large" color="#ff0000"></ActivityIndicator>
            }
    </ScrollView>
  );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#eee'
    },
    header: {
        flexDirection: 'row',
        backgroundColor: '#eee'
    },
    returnArrowView:{
        flex: 1,
        marginTop: 10
    },
    productImageView: {
        flex: 2
    },
    checkListView: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    checkListContainer: {
        marginTop: 10,
        padding: 7,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8
    },
    checkList: {
        width: 50, 
        height: 60, 
    },
    price: {
        height: 30,
        borderTopLeftRadius: 9,
        backgroundColor: '#c40404',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    priceText: {
        color: 'white',
        fontSize: 20
    },
    title: {
        backgroundColor: 'red',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        paddingVertical: 5,
        paddingHorizontal: 10
    },
    titleText: {
        fontSize: 24,
        color: 'white'
    },
    buyButton: {
        marginVertical: 20,
        width: '80%',
        height: 60,
        backgroundColor: '#edfa02',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12
    },
    buyButtonText: {
        fontSize: 34,
        color: 'red'
    },
    comprouComprou: {
        width: '100%'
    },
    comprouComprouTitle: {
        height: 35,
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'red',
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    comprouComprouText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold'
    },
    disclaimerView: {
        width: '100%',
        paddingLeft: 5
    },
    disclaimer: {
        fontSize: 12
    }
});